package br.com.webtask.aula.controller.service;

import br.com.webtask.aula.controller.service.UserService;
import br.com.webtask.aula.domain.model.Task;
import br.com.webtask.aula.domain.model.UserClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;


import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@SpringBootTest
class UserServiceTest {

    @Autowired
    UserService service;

    UserClient user;

    @BeforeEach
    void setUp() {
        user = new UserClient(1l,"Rennan Damiao",null,null,null,true,new ArrayList<Task>());
        service.salvar(user);


//
    }

    @AfterEach
    void tearDown() {

    }
    @Test
    public void test(){

        UserClient temporario = null;
        try {
            temporario = service.buscarUsuario(1l);
            System.out.println(temporario.getName());
        } catch (Exception e) {
            System.out.println( e.getMessage());
        }



    }
    @Test
    public void test2(){

        UserClient temporario = null;
        try {
            temporario = service.buscarUsuario(7l);
            System.out.println(temporario.getName());
        } catch (Exception e) {
            System.out.println( e.getMessage());
        }
    }
//
}