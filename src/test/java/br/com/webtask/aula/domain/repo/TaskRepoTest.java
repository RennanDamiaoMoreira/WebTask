package br.com.webtask.aula.domain.repo;


import br.com.webtask.aula.domain.model.Task;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;



import java.time.LocalDate;
import java.util.List;


@SpringBootTest
public class TaskRepoTest {
    @Autowired
    private TaskRepo tasks;
    Task t1,t2;

    @BeforeEach
    void setUp() {
        System.out.println("criando bases");
        t1=new Task(1L,"Estudar", LocalDate.now().minusDays(2),null,null);
        t2=new Task(2L,"dormir", LocalDate.now().minusDays(2),null,null);
        tasks.save(t1);
        tasks.save(t2);
    }

    @AfterEach
     void tearDown() {
        System.out.println("apagando bases");
        tasks.deleteAll();
    }
    @Test
    public void testeInclusao(){
        List<Task> tLista=tasks.findByTaskDescription("Estudar");
        System.out.println(tLista.size());
        System.out.println("entrei aqui");

        Assertions.assertEquals(1,tLista.size());
    }

    @Test
    public void testeContido(){
        List<Task>tLista=tasks.findByTaskDescription("dormir");
        Task teste = tLista.get(0);
        System.out.println(teste.getTaskDescription());
        Assertions.assertEquals("dormir",teste.getTaskDescription());
    }
}