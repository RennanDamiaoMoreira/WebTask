package br.com.webtask.aula.domain.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest

class TaskTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }
    @Test
    public void verificarSeUmaTaskAtrasadaEstaNesteStatus(){
        Task t = new Task(1l,"Estudar", LocalDate.now().minusDays(2),null,null);
        EStatus status = t.getStatus();
        Assertions.assertEquals(EStatus.ATRASADO,status);
        System.out.println(status.getDescricao());
    }
    @Test
    public void verificarSeUmaTaskNovaaEstaNesteStatus(){
        Task t = new Task(1l,"Estudar", LocalDate.now(),null,null);
        EStatus status = t.getStatus();
        Assertions.assertEquals(EStatus.NOVO,status);
        System.out.println(status.getDescricao());
    }@Test
    public void verificarSeUmaTaskAtrasadaConcluidaEstaNesteStatus(){
        Task t = new Task(1l,"Estudar", LocalDate.now().minusDays(2),LocalDate.now(),null);
        EStatus status = t.getStatus();
        Assertions.assertEquals(EStatus.CONCLUIDO_ATRASADO,status);
        System.out.println(status.getDescricao());
    }@Test
    public void verificarSeUmaTaskconcluidaEstaNesteStatus(){
        Task t = new Task(1l,"Estudar", LocalDate.now().minusDays(2),LocalDate.now().minusDays(2),null);
        EStatus status = t.getStatus();
        Assertions.assertEquals(EStatus.CONCLUIDO_PRAZO,status);
        System.out.println(status.getDescricao());
    }
}