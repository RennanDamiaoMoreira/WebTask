package br.com.webtask.aula.domain.repo;

import br.com.webtask.aula.domain.model.Task;
import br.com.webtask.aula.domain.model.UserClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@Transactional

class UserRepoTest {
@Autowired
private UserRepo users;

UserClient u1,u2,u3;
    @BeforeEach
    void setUp() {
        System.out.println("Criando base");
        u1=new UserClient(1L,"Rennan Damiao","11122233344","123@gmail.com","123",true,new ArrayList<Task>());
        u2=new UserClient(2L,"Sergi","22233344455","1234@gmail.com","123",true,new ArrayList<Task>());
        u3=new UserClient(3L,"Josh","33344455566","12513@gmail.com","123",true,new ArrayList<Task>());
        users.save(u1);
        users.save(u2);
        users.save(u3);
        System.out.println(users.count());

    }

    @AfterEach
    void tearDown() {
        users.deleteAll();
    }

    @Test
    public void test(){
      Optional<UserClient> usuario = users.findByName("Rennan Damiao");
      if (usuario.isPresent()){
          System.out.println(usuario.get().getName());
      }
        usuario = users.findByName("carlos");
        if (usuario.isPresent()){
            System.out.println(usuario.get().getName());
        }
    }
    @Test
    public void test2(){
            Optional<UserClient> usuario = users.findByCpf("33344455566");
            if (usuario.isPresent()){
                System.out.println(usuario.get().getName());
            }
            usuario = users.findByCpf("11122233344");
            if (usuario.isPresent()){
                System.out.println(usuario.get().getName());
            }

    }
}